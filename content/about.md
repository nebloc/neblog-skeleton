---
title: "About"
date: 2020-03-02T14:31:14Z
draft: False
---
```
$ whoami
> Ben Coleman
```
Programmer, photographer, hates writing about himself.

Me:
![me.jpg](https://storage.googleapis.com/nebloccom/Collections/Scotland%20Film%202019/Inversander-38.jpg)
